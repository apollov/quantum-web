import asyncio
import contextlib
import json

import aiodocker
from channels.generic.websocket import AsyncWebsocketConsumer


class CalculatorConsumer(AsyncWebsocketConsumer):
    """
    front -websocket-> back
    back -websocket-> container
    back <-message- container
    front <-message- back
    """

    _queue: asyncio.Queue
    _task: asyncio.Task
    _docker: aiodocker.Docker
    _container: aiodocker.docker.DockerContainer

    async def connect(self):
        self._queue = asyncio.Queue(maxsize=10)
        self._docker = aiodocker.Docker()
        self._task = asyncio.get_event_loop().create_task(self._run_a_loop())
        await self.accept()

    async def receive(self, text_data=None, bytes_data=None):
        with contextlib.suppress(asyncio.QueueFull):
            self._queue.put_nowait(1)

    async def _run_a_loop(self):
        try:
            while True:
                await self._queue.get()
                await self._do_a_barrel_roll()
        except asyncio.CancelledError:
            # maybe catching generic Exception class is way too broad, but we would want to delete a container
            # anyway, and since we're in cancelled coro, we have no reason to worry about whys, let's just try
            with contextlib.suppress(Exception):
                await self._container.stop()
            with contextlib.suppress(Exception):
                await self._container.delete()

    async def _do_a_barrel_roll(self):
        self._container = await self._docker.containers.create(
            config={
                'Image': 'quantum-web_worker',
                'TTY': True,
            },
        )
        await self._container.start()

        async for record in self._container.log(
                stdout=True, stderr=False, follow=True):
            # there might be multiple items in a single log
            parts = record.split('\r\n')
            for part in filter(None, parts):
                await self.send(json.dumps({"energy": float(part)}))

        await self._container.delete()

    async def disconnect(self, close_code):
        self._task.cancel()
