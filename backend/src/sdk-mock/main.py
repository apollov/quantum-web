import numpy as np

import qboard

Q = np.random.rand(30, 30) - 0.5

solver = qboard.solver(mode="bf")


def cb(dic):
    if dic["cb_type"] == qboard.constants.CB_TYPE_NEW_SOLUTION:
        energy = dic["energy"]
        print(energy)


solver.solve_qubo(Q, callback=cb, timeout=30, verbosity=0)
